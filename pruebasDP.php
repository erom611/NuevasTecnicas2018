<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class pruebasDP extends Controller
{
    //
    var $codigoPrueba;
    var $tipoPrueba;
    var $tiempoPrueba;
    var $tipoTranstorno;
    var $tipoFobia;
    var $tratamiento;

    function setNombre($codigoPrueba,$tipoPrueba,$tiempoPrueba,$tipoTranstorno,$tipoFobia,$tratamiento){
		$this -> codigoPrueba = $codigoPrueba;
        $this -> tipoPrueba = $tipoPrueba;
        $this -> tiempoPrueba = $tiempoPrueba;
        $this -> tipoTranstorno = $tipoTranstorno;
        $this -> tipoFobia = $tipoFobia;
        $this -> tratamiento = $tratamiento;
        
	}
	function getCodigoPrueba(){
		return $this->codigoPrueba;
	}
	function getTipoPrueba(){	
	return $this->tipoPrueba;
    }
    function getTiempoPrueba(){	
        return $this->tiempoPrueba;
    }
    function getTipoTranstorno(){	
        return $this->tipoTranstorno;
    }
    function getTipoFobia(){	
        return $this->tipoFobia;
    }
    function getTratamiento(){	
        return $this->tratamiento;
    }
    
}
